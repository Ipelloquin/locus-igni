﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class Tir : MonoBehaviour
{

    // Variable tir
    public float speed = 2;
    public bool shooted;
    public ParticleSystem feuHaut, feuDroite, feuBas, feuGauche;
    public bool un, deux, trois, quatre;
    
    // Variable temps
    public bool countdown = false;
    private float _tempsCdBase, tempsCD = 2.0f;
    private float _reservoir1, reservoir2 = 5.0f;

    public AudioSource flamethrower;
    // Start is called before the first frame update
    void Start()
    {
        flamethrower.Stop();
    }

    // Update is called once per frame
    void Update()
    {
  
        // Condition permettant de gérer le temps du countdown
        if (countdown == false)
        {
            _tempsCdBase -= Time.deltaTime;
            shooted = false;

            if (_tempsCdBase <= 0.0f)
            {
                _tempsCdBase = tempsCD;
                _reservoir1 = reservoir2;
                countdown = true;
            }
        }


        if (deux == false && trois == false && quatre == false)
        {
            // Condition pour tirer vers le haut
            if (Input.GetButtonDown("Fire1") && countdown)
            {
                //Rigidbody2D Feu = Instantiate(bullet2, new Vector3(transform.position.x, transform.position.y + position, transform.position.z), Quaternion.identity) as Rigidbody2D;
                 un = true;
                feuHaut.Play();
                shooted = true;
               
                _reservoir1 = -Time.deltaTime;

                /*if (_reservoir1 <= 0.0f)
                {
                    countdown = false;
                    feuHaut.Stop();
                }*/
            }

           
        }

        if (un)
        {
            if (Input.GetButtonUp("Fire1"))
            {
                feuHaut.Stop();
                shooted = false;
                un = false;
            }
        }


        if (un == false && trois == false && quatre == false)
        {
            // Condition pour tirer vers la droite
            if (Input.GetButtonDown("Fire2") && countdown )
            {
                /*Rigidbody2D Feu = Instantiate(bullet1, new Vector3(transform.position.x + position, transform.position.y, transform.position.z) , Quaternion.identity) as Rigidbody2D;
                Feu.transform.localRotation = Quaternion.Euler(0, 0, 0);
                Feu.transform.eulerAngles = gameObject.transform.eulerAngles;*/
                deux = true;
                feuDroite.Play();
                shooted = true;
              
                _reservoir1 = -Time.deltaTime;

                /* if (_reservoir1 <= 0.0f)
                 {
                     countdown = false;
                     feuDroite.Stop();
                 }*/
            }

           
        }

        if (deux == true)
        {
            if (Input.GetButtonUp("Fire2"))
            {
                feuDroite.Stop();
                shooted = false;
                deux = false;
            }
        }


        if (un == false && deux == false && quatre == false)
        {


            // Condition pour tirer vers le bas
            if (Input.GetButtonDown("Fire3") && countdown)
            {
                /*Rigidbody2D Feu = Instantiate(bullet2, new Vector3(transform.position.x, transform.position.y - position, transform.position.z), Quaternion.identity) as Rigidbody2D;
                Feu.transform.localRotation = Quaternion.Euler(0, 0, -90);
                Feu.transform.eulerAngles = gameObject.transform.eulerAngles;*/
                 trois = true;
                feuBas.Play();
                shooted = true;
               
                _reservoir1 = -Time.deltaTime;

                /* if (_reservoir1 <= 0.0f)
                 {
                     countdown = false;
                     feuBas.Stop();
                 }*/
            }

        }

        if (trois)
        {
            if (Input.GetButtonUp("Fire3"))
            {
                feuBas.Stop();
                shooted = false;
                trois = false;
            }
        }

        if (un == false && deux == false && trois == false)
        {


            // Condition pour tirer vers la gauche
            if (Input.GetButtonDown("Fire4") && countdown)
            {
                /*Rigidbody2D Feu = Instantiate(bullet1, new Vector3(transform.position.x - position, transform.position.y, transform.position.z), Quaternion.identity) as Rigidbody2D;
                Feu.transform.localRotation = Quaternion.Euler(0, 0, 180);
                Feu.transform.eulerAngles = gameObject.transform.eulerAngles;*/
                quatre = true;
                feuGauche.Play();
                shooted = true;
                
                _reservoir1 = -Time.deltaTime;

                /* if (_reservoir1 <= 0.0f)
                 {
                     countdown = false;
                     feuGauche.Stop();
                 }*/
            }
        }

        if (quatre == true)
        {
            if (Input.GetButtonUp("Fire4"))
            {
                feuGauche.Stop();
                shooted = false;
                quatre = false;
            }
        }

        if (Input.GetButtonDown("Fire1")|| Input.GetButtonDown("Fire2")|| Input.GetButtonDown("Fire3")|| Input.GetButtonDown("Fire4"))
        {
            flamethrower.Play();
        }

        if (Input.GetButtonUp("Fire1")|| Input.GetButtonUp("Fire2")|| Input.GetButtonUp("Fire3")|| Input.GetButtonUp("Fire4"))
        {
            flamethrower.Stop();
        }
        
    }
}

