﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using Random = UnityEngine.Random;

public class playerMove : MonoBehaviour
{
    public LayerMask ground;
    public Rigidbody2D rb;
    public float moveInput,speed,force,jumpTimer,jumpTime,propulsion;
    public bool isGrounded;
    public AudioClip[] footstep = new AudioClip[5];
    public bool wallTouch;
    public float wallSlide = 0.5f;
    public bool isPropulsed;
    public bool fireUp, fireRight, fireDown, fireLeft,fire;
    public GameObject weapon;
    public float timerFootstep;

    public Animator animator;
    public Sprite playerLeft;
    public Sprite jump;
    public Sprite playerRight;

    public Sprite playerShootRight;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        isPropulsed = true;
        animator.SetFloat("Speed",0);
    }

    // Update is called once per frame
    void Update()
    {
        fireUp = weapon.GetComponent<Tir>().un;
        fireRight = weapon.GetComponent<Tir>().deux;
        fireDown = weapon.GetComponent<Tir>().trois;
        fireLeft = weapon.GetComponent<Tir>().quatre;
        fire = weapon.GetComponent<Tir>().shooted;
        moveInput = Input.GetAxis("Horizontal");
        if (wallTouch == false)
        {
            transform.Translate(new Vector2(moveInput * speed, 0) * Time.deltaTime); // Mouvement du joueur
        }

        
        if (rb.velocity.y < 0) // wallslide monté
        {
            if (isGrounded == false)
            {
                Debug.DrawRay(transform.position, new Vector3(moveInput , 0, 0), Color.red);
                if (Physics2D.Raycast(transform.position, new Vector2(moveInput, 0), 0.5f, ground)
                ) //verification s'il est sur un mur
                {
                    rb.velocity = new Vector2(moveInput * speed, -5);
                    wallTouch = true;

                }
            }
        }
        else
        {
            wallTouch = false;
        }

        if (rb.velocity.y > 0.01f) // wallslide en descente
        {
            if (Physics2D.Raycast(transform.position, new Vector2(moveInput, 0), 0.5f, ground)
            ) // verification s'il est sur un mur
            {
                if (wallSlide >= 0) // glisse sur le mur
                {
                    rb.velocity = new Vector2(moveInput * speed, 3);
                    wallSlide -= Time.deltaTime;
                }
            }
        }

        if (jumpTimer <= 0)
        {
            if (Input.GetButtonDown("Jump") && isGrounded) // Saut du joueur
            {
                rb.AddForce(new Vector2(0, force), ForceMode2D.Impulse); // propulsion pour le saut
                jumpTimer = jumpTime;
                wallSlide = 0.1f;
                isGrounded = false;
              
            }
        }
       
        if (Physics2D.Raycast(transform.position, new Vector2(0.22f, -1), 0.7f, ground) ||
            Physics2D.Raycast(transform.position, new Vector2(-0.22f, -1), 0.7f, ground)
        ) // verification s'il est sur le sol
        {
            isGrounded = true;
            isPropulsed = true;
        }
        else
        {
            isGrounded = false;
        }

      
        
        if (jumpTimer >= 0) // timer pour le saut
        {
            jumpTimer -= Time.deltaTime;

        }

        if (isGrounded == false) //timer pour le wallslide 
        {
            wallSlide -= Time.deltaTime;
        }
        

        if (Input.GetButtonDown("Fire1")) // propulsion vers le bas
        {
            if (isPropulsed) // s'il ne c'est pas déjà propulsé
            {
                rb.AddForce(new Vector2(0, -propulsion - 2), ForceMode2D.Impulse); // propulsion
                isPropulsed = false;
                
            }
        }

        if (Input.GetButtonDown("Fire2"))  // propultion vers la gauche
        {
            if (isPropulsed) // s'il ne c'est pas déjà propulsé
            {
                if (isGrounded == false)
                {
                    rb.velocity = new Vector2(-propulsion, rb.velocity.y); // propulsion en l'air
                    isPropulsed = false;
                }

                if (isGrounded)
                {
                    rb.velocity = new Vector2(-propulsion / 1.5f, rb.velocity.y); // propulsion au sol
                    isPropulsed = false;
                    GetComponent<SpriteRenderer>().sprite = playerShootRight;
                }
            }
        }

        if (Input.GetButtonDown("Fire3"))  // propulsion ver le haut
        {
            if (isPropulsed) // s'il ne c'est pas déjà propulsé
            {
                rb.velocity = new Vector2(0, propulsion + 2); // propulsion
                isPropulsed = false;
            }
        }

        if (Input.GetButtonDown("Fire4")) // propulsion vers la droite
        {
            if (isPropulsed) // s'il ne c'est pas déjà propulsé
            {
                if (isGrounded == false)
                {
                    rb.velocity = new Vector2(propulsion, rb.velocity.y); // propulsion an l'air 
                    isPropulsed = false;
                }

                if (isGrounded)
                {
                    rb.velocity = new Vector2(propulsion / 1.5f, rb.velocity.y); // propulsion au sol
                    isPropulsed = false;
                  
                }

            }
        }

        if (isGrounded == false)
        {
          //  GetComponent<SpriteRenderer>().sprite = jump;
          animator.SetBool("Jump",true);
          animator.Play("Player_Saut");
        }
        else
        {
            animator.SetBool("Jump",false);
        }

      
            if (moveInput > 0)
            {
              //  GetComponent<SpriteRenderer>().sprite = playerRight; // retourne le joueur
                animator.SetFloat("Speed",1);
                animator.SetBool("Left",false);
                if (speed <= 0) // inverse la vitesse
                {
                    speed *= -1;
                }
            }
            else if (moveInput < 0)
            {
              //  GetComponent<SpriteRenderer>().sprite = playerLeft; // retourne le joueur
              animator.SetFloat("Speed",-1);
              animator.SetBool("Left",true);
            }
           else
           {
               animator.SetFloat("Speed",0f);
           }
           
        

        if (fireUp)
        {
            animator.SetBool("un",true);
        }
        else
        {
            animator.SetBool("un",false);
        }

        if (fireRight)
        {
            animator.SetBool("deux",true);
        }
        else
        {
            animator.SetBool("deux",false);
        }

        if (fireDown)
        {
            animator.SetBool("trois",true);
        }
        else
        {
            animator.SetBool("trois",false);
        }

        if (fireLeft)
        {
            animator.SetBool("quatre",true);
        }
        else
        {
            animator.SetBool("quatre",false);
        }
        if (isGrounded) // vérifie s'il est sur le sol
        {
            if (moveInput > 0 || moveInput < 0) // vérifie si le joueur bouge
            {
                if (timerFootstep <= 0) // son de bruit de pas 
                {
                    int rdm = Random.Range(0, 5);
                    AudioSource.PlayClipAtPoint(footstep[rdm], transform.position);
                    timerFootstep = 0.5f;
                }
                if (timerFootstep >=0)
                {
                    timerFootstep -= Time.deltaTime;
                }
            }
        }
    }
}