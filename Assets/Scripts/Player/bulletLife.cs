﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletLife : MonoBehaviour
{
    // Variable //
    public float lifeTime = 5;
    
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        // Détruit l'objet
        Destroy(gameObject, lifeTime);
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        //rb.velocity = transform.TransformDirection(new Vector3(5, 0, 0));
        /*  if (rb.velocity.x != 5 )
          {
              Destroy(gameObject);
          }*/
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("ennemie"))
        {
            Debug.Log("Ennemie touché");
            Destroy(gameObject);
        }
        if(other.gameObject.CompareTag("ground"))
        {
            Destroy(gameObject);
        }
    }
}
