﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BurningGround : MonoBehaviour
{
    public Tilemap burningTilemap;
    public bool isEnnemyBurning;
    public Vector3Int touch;
    public Vector3 test;
    public Vector3 decalage = new Vector3(0,0.64f,0);
    public TileBase burningGround;
    public bool isEnnemyFall;
    public Tilemap burn;
    public TileBase normalGround;
    public bool isBurning;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (normalGround == false)
        {
            isBurning = false;
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("ennemie"))
        {
            test = other.gameObject.transform.position;
           touch = burningTilemap.WorldToCell(test - decalage);
           
           isEnnemyBurning = other.gameObject.GetComponent<ennemiesZombie>().isBurning;
            isEnnemyFall = other.gameObject.GetComponent<ennemiesZombie>().isFall;
            
            if (isEnnemyBurning)
            {
                if (isEnnemyFall == false)
                {
                    isBurning = true;
                    burn.SetTile(touch,burningGround);
                    burningTilemap.SetTile(touch, null);
                }
            }
        }
    }
}
