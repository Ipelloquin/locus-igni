﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class EnnemyState : MonoBehaviour
{
    private State currentState;
    

    private void Start()
    {
        SetState(new NotOnFire(this));
    }

    private void Update()
    {
        currentState.Tick();
    }

    public void SetState(State state)
    {
        currentState = state;
    }
    
}

public abstract class State
{
    protected EnnemyState gameState;

    public abstract void Tick();
    
    public State(EnnemyState gameState)
    {
        this.gameState = gameState;
    }
}

public class NotOnFire : State
{
    private bool change = false;
    public NotOnFire(EnnemyState gameState) : base(gameState){}
    public override void Tick()
    {
        Debug.Log("Not On Fire");

        if (change == true)
        {
            Debug.Log("shooted");
            gameState.SetState(new InFire(gameState));
        }
    }
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            change = true;
        }
    }
}

public class InFire : State
{
    public InFire(EnnemyState gameState) : base(gameState){}
    
    public GameObject obj = new GameObject();
    public override void Tick()
    {
        Debug.Log("In Fire");
        
        GameObject.Destroy(obj, 1.5f);
    }
}

