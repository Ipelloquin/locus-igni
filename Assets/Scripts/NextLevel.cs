﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public int ennemieNumber;

    public LayerMask player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ennemieNumber = GameObject.FindGameObjectsWithTag("ennemie").Length; // nombre d'ennemi sur le niveau
        if (ennemieNumber > 0) // enlève les collision tant qu'il y a des ennemi
        {
            GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            if (Physics2D.CircleCast(transform.position,0.5f,Vector2.zero,0,player)) // passe au niveau suivant si le joueur est proche de la porte et qu'il n'y a plus d'ennemi
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
}
