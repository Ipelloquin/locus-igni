﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemieShoot : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(0,5) * Time.deltaTime); // vitesse du tir
        Destroy(gameObject, 5); //destruction du tir après 5 seconde
    }
}
