﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemieExplosif : MonoBehaviour
{

    public bool isBurning, isFall;

    public float turningValue, frontValue, speed, time, timerRotation, life;

    public Rigidbody2D rb;

    public LayerMask ground, jump, turn;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        life = gameObject.GetComponent<lifeEnnemy>().life; // donne la vie du monstre
        isBurning = gameObject.GetComponent<BurnPropagation>().isBurning; // vérifie s'il brule
        Debug.DrawRay(transform.position, new Vector3( turningValue,-0.5f,0), Color.red);
        Debug.DrawRay(transform.position, new Vector3( frontValue,0,0), Color.green);
        if (isBurning == false) // pattern quand il ne brule pas
        {
           
            if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)) // avance sur le sol
            {
                rb.velocity = new Vector2(speed, -0.2f);
            }
            else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, jump)) // saut entre plateforme
            {
                rb.velocity = new Vector2(speed * 1.7f, 2);
            }
            else if(Physics2D.Raycast(transform.position,new Vector2(turningValue,-1), 1f, turn))
            {
                if (time <= 0) // quand timer fini et qu'il rencontre un trou change de direction pour l'ennemie 
                {
                    transform.Rotate(new Vector3(0,180,0), Space.Self);
                    speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                    time = timerRotation;
                }
            }
            if (time >= 0) // timer
            {
                      
                time -= Time.deltaTime;
            }

            if (Physics2D.Raycast(transform.position, new Vector2(frontValue, 0), 0.5f, ground)) // s'il rencontre un mur change de direction
            {
                transform.Rotate(new Vector3(0,180,0), Space.Self);
                speed *= -1;
                turningValue *= -1;
                frontValue *= -1;
            }
        }

        if (isBurning)
        {
            if (life > 10)
            {
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 0.4f, ground)) //s'il est sur le sol avance'
                {
                    rb.velocity = new Vector2(speed * 1.5f, -0.2f);
                    isFall = false;
                }
                else //sinon tombe
                {
                    rb.velocity = new Vector2(speed * 0.5f, rb.velocity.y);
                    isFall = true;
                }
            }
            
            if (life <= 10) // si ça vie est faible ralentie
            {
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 0.4f, ground))
                {
                    rb.velocity = new Vector2(speed * .5f, -0.2f);
                    isFall = false;
                }
            }
            
            gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
}
