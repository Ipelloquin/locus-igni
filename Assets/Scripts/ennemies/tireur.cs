﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class tireur : MonoBehaviour
{

    public GameObject player;
    public float timerShoot, timeShoot,speed,turningValue,frontValue;
 
    public GameObject bullet;
    public int rotationFront;

    public bool isBurning;

    public Rigidbody2D rb;

    public Sprite shooterLeft;
    public Sprite shooterRight;

    public LayerMask ground,jump,turn;

 

    public int life;

    public bool horizontal;
    // Start is called before the first frame update
    void Start()
    {
        timerShoot = timeShoot;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        isBurning = gameObject.GetComponent<BurnPropagation>().isBurning; // vérifie s'il brule
        life = gameObject.GetComponent<lifeEnnemy>().life; //donne sa vie
        if (isBurning == false) // pattern quand il ne brule pas
        {
           
            if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)) // avance sur le sol
            {
                rb.velocity = new Vector2(speed, -0.2f);
            }
            else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, jump)) // saut entre plateforme
            {
                rb.velocity = new Vector2(speed * 1.7f, 1);
            }
            else if(Physics2D.Raycast(transform.position,new Vector2(turningValue,-1), 1f, turn)) // tourne s'il rencontre un trou
            {
                speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                   
              
            }

            if (Physics2D.Raycast(transform.position, new Vector2(frontValue, 0), 0.5f, ground)) // s'il rencontre un mur change de direction
            {
                speed *= -1;
                turningValue *= -1;
                frontValue *= -1;
            }
        }
        if (frontValue >= 0)
        {
            rotationFront = -90;
        }
        else
        {
            rotationFront = 90;
        }
        
        if (isBurning == false) // s'il ne brule pas
        {
            if (timerShoot <= 0)
            {
                if (Vector3.Distance(new Vector3(0, transform.position.y, 0),
                        new Vector3(0, player.transform.position.y, 0)) <= 0.1f) //tire devant lui
                {
                    Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, rotationFront))); //tire
                    timerShoot = timeShoot;
                    horizontal = true;
                
                }
                
                if (horizontal == false)
                {

                  
                    if (transform.position.y <= player.transform.position.y) // tire vers le haut
                    {
                        Instantiate(bullet, transform.position, Quaternion.identity); //tire
                        timerShoot = timeShoot;
                    }
                    else if (transform.position.y >= player.transform.position.y) // tire vers le bas
                    {
                        Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, 180))); //tire
                        timerShoot = timeShoot;
                    }
                }

               
                
            }
            
            if (speed <= 0)
            {
                GetComponent<SpriteRenderer>().sprite = shooterLeft;  // retourne l'ennemi
                Debug.Log("test25");
            }

            if (speed >= 0)
            {
                GetComponent<SpriteRenderer>().sprite = shooterRight; // retourne l'ennemi
                Debug.Log("test45");
            }
            
            Debug.Log(speed);
            if (timerShoot >= 0) //timer
            {
                timerShoot -= Time.deltaTime;
                horizontal = false;
            }
        }

        if (isBurning) // s'il brule
        {
           
            if (life > 2) // beaucoup de vie
            {
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)) // avance sur le sol
                {
                    rb.velocity = new Vector2(speed *1.5f, -0.2f);
                }
                else // se retourne en bout de plateforme 
                {
                
                    speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                    
           
                }

                if (Physics2D.Raycast(transform.position, new Vector2(frontValue, 0), 0.5f, ground)) // s'il rencontre un mur change de direction
                {
                  
                    speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                }
                
                if (timerShoot <= 0) // tire dans toute les direction
                {
                    Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
                    Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, 90)));
                    Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
                    Instantiate(bullet, transform.position, Quaternion.Euler(new Vector3(0, 0, 270)));
                    timerShoot = 1;
                }

                if (timerShoot >= 0) //timer
                {
                    timerShoot -= Time.deltaTime;
                }
                
                if (speed <= 0) // retourne l'ennemi
                {
                    GetComponent<SpriteRenderer>().sprite = shooterLeft; 
                }

                if (speed >= 0) // retourne l'ennemi
                {
                    GetComponent<SpriteRenderer>().sprite = shooterRight;
                }
            }
            gameObject.GetComponent<SpriteRenderer>().color = Color.magenta;
        }
    }
}