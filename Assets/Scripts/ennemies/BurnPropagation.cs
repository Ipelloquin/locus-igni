﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnPropagation : MonoBehaviour
{
    public float rayon;
    public LayerMask ennemies;
    public bool isBurning = false;
   // public ParticleSystem fireUp, fireDown, fireLeft, fireRight;
    public LayerMask bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        RaycastHit2D circleCast = Physics2D.CircleCast(transform.position, rayon, new Vector2(0, 0),0f,ennemies); 
        // tracahe d'un cercle qui vérifie si un ennemie entre en contact avec un ennemie

        if (circleCast.collider.CompareTag("ennemie") && circleCast.collider.gameObject.GetComponent<BurnPropagation>().isBurning )// vérifie si l'ennemie brule
        {
            Debug.Log("euh ok");
            isBurning = true; // brule cet ennemie
        }
    
    
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("bullet")) // vérifie si l'ennemie touche un tir
        {
            Debug.Log("bruler");
            isBurning = true;// le brule
        }

        if (other.gameObject.CompareTag("burning"))// vérifie si l'ennemie touche un sol qui brule
        {
            isBurning = true;// le brule
        }
    }

    private void OnParticleCollision(GameObject other) // prend feu au contact d'une particule
    {
        isBurning = true;
    }
}

