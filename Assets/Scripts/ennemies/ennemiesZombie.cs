﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;

public class ennemiesZombie : MonoBehaviour
{
    public Rigidbody2D rb;
    public LayerMask ground,jump,turn;
    public float speed, turningValue, frontValue;
    public bool isFall;
    public bool isBurning;
    public int life ;

    public Sprite zombieLeft;

    public Sprite zombieRight;

    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
 
        
    }

    // Update is called once per frame
    void Update()
    {
        life = gameObject.GetComponent<lifeEnnemy>().life; // donne la vie du monstre
        isBurning = gameObject.GetComponent<BurnPropagation>().isBurning; // vérifie s'il brule
        Debug.DrawRay(transform.position, new Vector3( turningValue,-0.5f,0), Color.red);
        Debug.DrawRay(transform.position, new Vector3( frontValue,0,0), Color.green);
        Debug.DrawRay(new Vector2(transform.position.x ,transform.position.y - 0.52f), new Vector3(0.2f,0,0), Color.cyan);
        if (isBurning == false) // pattern quand il ne brule pas
        {
           
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)) // avance sur le sol
                {
                    rb.velocity = new Vector2(speed, -0.2f);
                }
                else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, jump)) // saut entre plateforme
                {
                    rb.velocity = new Vector2(speed * 1.7f, 1);
                }
                else if(Physics2D.Raycast(transform.position,new Vector2(turningValue,-1), 1f, turn))// quand timer fini et qu'il rencontre un trou change de direction pour l'ennemie 
                {
                
                       
                       // transform.Rotate(new Vector3(0,180,0), Space.Self);
                        speed *= -1;
                        turningValue *= -1;
                        frontValue *= -1;
                  
                      
                }
          

               if (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.05f), new Vector2(frontValue, 0), 0.25f, ground)) // s'il rencontre un mur change de direction
                {
                   
                    speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                    
                 
                
                }
               
        }

        if (speed <= 0) // retourne l'ennemi
        {
            //    GetComponent<SpriteRenderer>().sprite = zombieLeft; 
            animator.SetFloat("verif",2);
        }

        if (speed >= 0) // retourne l'ennemi
        {
          //  GetComponent<SpriteRenderer>().sprite = zombieRight;
            animator.SetFloat("verif",-2);
        }
 
        
        if (isBurning) //pattern quand il brule
        {
            if (life > 2)
            {
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 0.7f, ground)) //s'il est sur le sol avance'
                {
                    rb.velocity = new Vector2(speed * 1.5f, -0.2f);
                    isFall = false;
                }
                else //sinon tombe
                {
                    rb.velocity = new Vector2(speed * 0.5f, rb.velocity.y);
                    isFall = true;
                }
            }

            if (life <= 2) // si ça vie est faible ralentie
            {
                if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 0.4f, ground))
                {
                    rb.velocity = new Vector2(speed * .5f, -0.2f);
                    isFall = false;
                    animator.SetBool("dead", true);
                }
            }

            if (Physics2D.Raycast(new Vector2(transform.position.x ,transform.position.y - 0.05f), new Vector2(frontValue, 0), 0.25f, ground) 
            ) // s'il rencontre un mur change de direct
            {
                speed *= -1;
                turningValue *= -1;
                frontValue *= -1;
            }

            if (Physics2D.Raycast(new Vector2(transform.position.x ,transform.position.y - 0.48f), new Vector2(frontValue, 0), 0.2f, ground))
            {
                 speed *= -1;
            
           
              
                 turningValue *= -1;
                 frontValue *= -1;
            }
  
           
            gameObject.GetComponent<SpriteRenderer>().color = Color.red;
          
        }
    }

   
}
