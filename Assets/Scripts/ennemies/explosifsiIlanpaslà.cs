﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosifsiIlanpaslà : MonoBehaviour
{
     public float life;

    public bool isBurning;

    public float turningValue,frontValue,speed,time, jumpTime;

    public Rigidbody2D rb;
    
    public LayerMask ground,jump,turn;

    public GameObject player;

    public bool isTimerFinish;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        life = gameObject.GetComponent<lifeEnnemy>().life; // donne la vie du monstre
        isBurning = gameObject.GetComponent<BurnPropagation>().isBurning; // vérifie s'il brule
        Debug.DrawRay(transform.position, new Vector3( turningValue,-0.5f,0), Color.red);
        Debug.DrawRay(transform.position, new Vector3( frontValue,0,0), Color.green);
        if (isBurning == false) // pattern quand il ne brule pas
        {
           
            if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)) // avance sur le sol
            {
                rb.velocity = new Vector2(speed, -0.2f);
            }
            else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, jump)) // saut entre plateforme
            {
                rb.velocity = new Vector2(speed * 1.7f, 2);
            }
            else if(Physics2D.Raycast(transform.position,new Vector2(turningValue,-1), 1f, turn))
            {
               
                    transform.Rotate(new Vector3(0,180,0), Space.Self);
                    speed *= -1;
                    turningValue *= -1;
                    frontValue *= -1;
                
            }

            if (Physics2D.Raycast(transform.position, new Vector2(frontValue, 0), 0.5f, ground)) // s'il rencontre un mur change de direction
            {
                transform.Rotate(new Vector3(0,180,0), Space.Self);
                speed *= -1;
                turningValue *= -1;
                frontValue *= -1;
            }
        }

        if (isBurning)
        {
            Debug.Log(1);
            if (life >= 5)
            {
                Debug.Log(2);
               

                   
                    if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, ground)
                    ) // avance sur le sol
                    {
                        rb.velocity = new Vector2(speed * 1.5f, -0.2f);
                        Debug.Log(3);
                    }
                    else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, jump)
                    ) // saut entre plateforme
                    {
                        rb.velocity = new Vector2(speed * 2.7f, 2);
                    }
                    else if (Physics2D.Raycast(transform.position, new Vector2(turningValue, -1), 1f, turn))
                    {

                        transform.Rotate(new Vector3(0, 180, 0), Space.Self);
                        speed *= -1;
                        turningValue *= -1;
                        frontValue *= -1;
                    }
                    
                    if (Physics2D.Raycast(transform.position, new Vector2(frontValue, 0), 0.5f, ground)) // s'il rencontre un mur change de direction
                    {
                        transform.Rotate(new Vector3(0,180,0), Space.Self);
                        speed *= -1;
                        turningValue *= -1;
                        frontValue *= -1;
                    }
                    
                if (time <= 0)
                {
                 
                    
                    if (player.transform.position.y >= transform.position.y)
                    {
                        
                            if (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.32f),
                                new Vector2(0, 1), 1.6f, ground))
                            {
                                rb.velocity = new Vector2(0, 1.64f);
                                

                            }

                            if (jumpTime >= 0)
                            {
                                jumpTime -= Time.deltaTime;
                            }

                            if (jumpTime <= 0)
                            {
                                time = 5;
                            }
                       
                    }
                }
            }
        }

        if (time >=0)
        {
            time -= Time.deltaTime;
        }
    }
}