﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifeEnnemy : MonoBehaviour
{
    public int life;
    private bool _isBurning;
    private float _oneSecond = 1;
    public ParticleSystem fire;
    public AudioClip deathClip;
    // Start is called before the first frame update
    void Start()
    {
        fire.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        _isBurning = gameObject.GetComponent<BurnPropagation>().isBurning; // reprend la valeur isBurning (vérifie s'il brule)
        
        if (_isBurning) //s'il brule 
        {
            
            if (_oneSecond >= 0)//timer pour infliger des dégâts
            {
                _oneSecond -= Time.deltaTime;
            }

            if (_oneSecond <= 0) // infligement des dégâts
            {
                fire.Play();
                life--;
                _oneSecond = 1;
            }
        }

        if (life <= 0) // destruction quand il n'a plus de vie
        {
            fire.Stop();
            AudioSource.PlayClipAtPoint (deathClip, transform.position); // son a sa mort
            Destroy(gameObject);
        }
    }
}
