﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FireGroundSoundS : MonoBehaviour
{
    public GameObject fireSound;

    public GameObject burn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (burn.active)
        {
            fireSound.SetActive(true);
        }
    }
}
