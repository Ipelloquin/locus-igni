﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public float timer;
    public float timerSpawn;
    public float orientationRight;
    public float orientationLeft;
    public GameObject bullet;
   
    // Start is called before the first frame update
    void Start()
    {
        timerSpawn = 1;
    }

    // Update is called once per frame
    void Update()
    { 
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timerSpawn >= 0)
        {
            timerSpawn -= Time.deltaTime;
        }
       /* if (timer <= 0)
        {
           
            if (timerSpawn <= 0)
            {
                var rotateRight = transform.rotation.eulerAngles.z;
                var rotateLeft = transform.rotation.eulerAngles.z;
               
                timerSpawn = 1;
                for (int i = 0; i < spawnShootRight.Length; i++)
                {
                    orientationRight = Random.Range(0, 180);
                    rotateRight = orientationRight;
                    Instantiate(bullet, spawnShootRight[i].transform.position, Quaternion.Euler(new Vector3(0,0,rotateRight)));
                }

                for (int i = 0; i < spawnShootLeft.Length; i++)
                {
                    orientationLeft = Random.Range(-180, 0);
                    rotateLeft = orientationLeft;
                    Instantiate(bullet, spawnShootLeft[i].transform.position, Quaternion.Euler(new Vector3(0, 0, rotateLeft)));
                }
               
            }
        }*/
    }

    public void OnCollisionEnter2D(Collision2D other)
    { 
        if (other.collider.CompareTag("ennemie"))
        {
           
           SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        } 
        
        if (other.collider.CompareTag("burning")) 
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
          
        }

        if (other.collider.CompareTag("ennemieshoot"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
