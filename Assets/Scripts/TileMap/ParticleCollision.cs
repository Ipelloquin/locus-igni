﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollision : MonoBehaviour
{
    
    public ParticleSystem part;
    
    public List<ParticleCollisionEvent> collisionEvent = new List<ParticleCollisionEvent>();

    public Vector3 positionParticle;
    
    // Start is called before the first frame update
    void Start()
    {
        part = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvent = part.GetCollisionEvents(other,collisionEvent); // créer un array de collision de particule

        for (int i = 0; i < numCollisionEvent; i++)
        {
            if (other.gameObject.CompareTag("burnable") )
            {
                positionParticle = collisionEvent[0].intersection; // trouve la position de la première particule qui touche l'objet
            }
        }
    }
}
