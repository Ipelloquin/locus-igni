﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening.Core;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BurningGroundPropagation : MonoBehaviour
{

    public Tilemap burn;
    public bool isEnnemyBurn;
    public ParticleSystem fireUp, fireDown, fireLeft, fireRight;
    public Vector3 touched;
    public TileBase burningTile;
    public Tilemap replaceable;
    public float timer = 1;
    public Vector3Int positionBurn;
    public GameObject burnObject;
    public static List<Burn> burnTile = new List<Burn>();
    public Vector3 decalageDown = new Vector3(0, 0.64f, 0);
    public Vector3 positionUp, positionDown, positionRight, positionLeft;
    public Vector3Int positionUpI, positionDownI, positionRightI, positionLeftI;
    public bool oneTime;
    public GameObject fireGround;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        

        if (timer <= 0)
        {

            EndOfTimer();
            oneTime = false;
            timer = 1;
        }

        positionUp = fireUp.GetComponent<ParticleCollision>().positionParticle + new Vector3(0,0.32f,0); // Récupération particule du lance flamme en fonction de la direction
        positionDown = fireDown.GetComponent<ParticleCollision>().positionParticle + new Vector3(0,-0.32f,0);// Récupération particule du lance flamme en fonction de la direction
        positionRight = fireRight.GetComponent<ParticleCollision>().positionParticle+ new Vector3(0.32f,0,0);// Récupération particule du lance flamme en fonction de la direction
        positionLeft = fireLeft.GetComponent<ParticleCollision>().positionParticle+ new Vector3(-0.32f,0,0);// Récupération particule du lance flamme en fonction de la direction
        
        if (positionUp != Vector3.zero) 
        {
            positionUpI = burn.WorldToCell(positionUp); //change la position de vector3 à vector3int
            
            burnTile.Add(new Burn(positionUpI, burn, replaceable, burningTile, fireGround,positionUp,oneTime)); // ajout d'une classe a la position toucher

          
            if (replaceable.HasTile(positionUpI)) // met une tile de feu a la position toucher
            {
                replaceable.SetTile(positionUpI, null);
                burn.SetTile(positionUpI, burningTile);
                Instantiate(fireGround, positionUp, transform.rotation);
                burnObject.SetActive(true);
                Debug.Log(positionUpI);
            }
        }
        
        if (positionDown != Vector3.zero)
        {
            positionDownI = burn.WorldToCell(positionDown); // change la position de vector3 à vector3int 
            
            burnTile.Add(new Burn(positionDownI, burn, replaceable, burningTile, fireGround,positionDown,oneTime)); // ajout d'une classe a la position toucher
         
         
            if (replaceable.HasTile(positionDownI)) // met une tile de feu a la position toucher
            {
                replaceable.SetTile(positionDownI, null);
                burn.SetTile(positionDownI, burningTile);
                burnObject.SetActive(true);
                Instantiate(fireGround, positionDown, transform.rotation);
                Debug.Log(positionDownI);
            }
        }
        
        if (positionRight != Vector3.zero) 
        {
            positionRightI = burn.WorldToCell(positionRight);// change la position de vector3 à vector3int
            
            burnTile.Add(new Burn(positionRightI, burn, replaceable, burningTile, fireGround,positionRight,oneTime));// ajout d'une classe a la position toucher
         
         
            if (replaceable.HasTile(positionRightI)) // met une tile de feu a la position toucher
            {
                replaceable.SetTile(positionRightI, null);
                burn.SetTile(positionRightI, burningTile);
                burnObject.SetActive(true);
                Instantiate(fireGround, positionRight, transform.rotation);
                Debug.Log(positionRightI);
            }
        }
        
        if (positionLeft != Vector3.zero)
        {
            positionLeftI = burn.WorldToCell(positionLeft); // change la position de vector3 à vector3int
            
            burnTile.Add(new Burn(positionLeftI, burn, replaceable, burningTile, fireGround,positionLeft,oneTime)); // ajout d'une classe a la position toucher
         
           
            if (replaceable.HasTile(positionLeftI)) // met une tile de feu a la position toucher
            {
                replaceable.SetTile(positionLeftI, null);
                burn.SetTile(positionLeftI, burningTile);
                burnObject.SetActive(true);
                Instantiate(fireGround, positionLeft, transform.rotation);
                Debug.Log(positionLeftI);
            }
        }
        
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
            
        }

    }


    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("ennemie"))// détecte la collision avec l'ennemi
        {
            touched = other.gameObject.transform.position - decalageDown;
            positionBurn = burn.WorldToCell(touched); // change sa postion en vector3int
            isEnnemyBurn = other.gameObject.GetComponent<BurnPropagation>().isBurning; // vérifie s'il brule

        }

       
        if (isEnnemyBurn)  
        {
            burnTile.Add(new Burn(positionBurn, burn, replaceable, burningTile, fireGround,touched,oneTime)); // créer un classe la ou il a toucher
            
          if (replaceable.HasTile(positionBurn)) // remplace les tiles qu'il touche
                {
                    Debug.Log("test");
                    replaceable.SetTile(positionBurn, null);
                    burn.SetTile(positionBurn, burningTile);
                    burnObject.SetActive(true);
                    Instantiate(fireGround, touched, transform.rotation);
                    Debug.Log(positionBurn);
                }
         
        }
    }
    public void EndOfTimer()
    {
        var hasBurned = new Burn[burnTile.Count]; // création array
        burnTile.CopyTo(hasBurned); // ajout des classe dans l'array

        foreach (var burned in hasBurned) // enlève la classe si elle ne peux plus propager de feu
        {
            if (!burned.Burning())
            {
                burnTile.Remove(burned);
            }
        }
      
        
    }
}




public class Burn
{
    private Vector3Int _up;
    private Vector3Int _down;
    private Vector3Int _right;
    private Vector3Int _left;
    public Tilemap replaceable;
    public Tilemap burnedTilemap;
    public TileBase burningTile;
    public Vector3 positionUp;
    public Vector3 positionDown;
    public Vector3 positionRight;
    public Vector3 positionLeft;
    public GameObject fire;
    public bool oneTime = true;
    public List<Burn> burningObject = new List<Burn>();

    public Burn(Vector3Int start, Tilemap replace, Tilemap canBurn, TileBase burningTiles, GameObject fireGround,
        Vector3 firePosition, bool time)
    {
        replaceable = canBurn;
        burnedTilemap = replace;
        burningTile = burningTiles;
        _right = start + Vector3Int.right;
        _left = start + Vector3Int.left;
        _up = start + Vector3Int.up;
        _down = start + Vector3Int.down;
        fire = fireGround;
        positionUp = firePosition + new Vector3(0, 0.32f, 0);
        positionDown = firePosition + new Vector3(0, -0.32f, 0);
        positionRight = firePosition + new Vector3(0.32f, 0, 0);
        positionLeft = firePosition + new Vector3(-0.32f, 0, 0);

    }

    public bool Burning()
    {
        bool continuer = false;


        if (replaceable != null)
        {


            if (replaceable.HasTile(_right + Vector3Int.right)) // vérifie s'il y a une tile a bruler a sa droite
            {

                replaceable.SetTile(_right, null);
                burnedTilemap.SetTile(_right, burningTile);
                /*  burningObject.Add(new Burn(_right, burnedTilemap, replaceable, burningTile, fire, positionRight, oneTime));
                  burningObject.Add(new Burn(_left, burnedTilemap, replaceable, burningTile, fire, positionLeft, oneTime));
                  burningObject.Add(new Burn(_up, burnedTilemap, replaceable, burningTile, fire, positionUp, oneTime));
                  burningObject.Add(new Burn(_down, burnedTilemap, replaceable, burningTile, fire, positionDown, oneTime));
                  GameObject.Instantiate(fire, positionRight, Quaternion.Euler(new Vector3(-90, 0, 0)));
                  BurningGroundPropagation.burnTile = burningObject;
                 */

                GameObject.Instantiate(fire, positionRight, Quaternion.Euler(new Vector3(-90, 0, 0)));



                _right += Vector3Int.right; // décalage pour la propagation
                _down = _right; // mise a jour de la position pour toute les autre fonction
                _up = _right;
                _left = _right;
                positionRight += new Vector3(0.32f, 0, 0);
                positionLeft = positionRight;
                positionDown = positionRight;
                positionUp = positionRight;
                continuer = true;
                Debug.Log(_right);

            }


            if (replaceable.HasTile(_left + Vector3Int.left)) // vérifie s'il y a une tile a bruler a sa gauche
            {

                replaceable.SetTile(_left, null);
                burnedTilemap.SetTile(_left, burningTile);
                /*   burningObject.Add(new Burn(_right, burnedTilemap, replaceable, burningTile, fire, positionRight, oneTime));
                   burningObject.Add(new Burn(_left, burnedTilemap, replaceable, burningTile, fire, positionLeft, oneTime));
                   burningObject.Add(new Burn(_up, burnedTilemap, replaceable, burningTile, fire, positionUp, oneTime));
                   burningObject.Add(new Burn(_down, burnedTilemap, replaceable, burningTile, fire, positionDown, oneTime));
                   GameObject.Instantiate(fire, positionLeft, Quaternion.Euler(new Vector3(-90, 0, 0)));
                   BurningGroundPropagation.burnTile = burningObject;
                  */

                GameObject.Instantiate(fire, positionLeft, Quaternion.Euler(new Vector3(-90, 0, 0)));
                oneTime = false;
                oneTime = true;
                _left += Vector3Int.left; // décalage pour la propagation
                _right = _left; // mise a jour de la position pour toute les autres fonction
                _down = _left;
                _up = _left;
                positionLeft += new Vector3(-0.32f, 0, 0);
                positionRight = positionLeft;
                positionDown = positionLeft;
                positionUp = positionLeft;
                continuer = true;

            }

            if (replaceable.HasTile(_up + Vector3Int.up)) //vérifie s'il y a une tile a bruler au dessus
            {
                replaceable.SetTile(_up, null);
                burnedTilemap.SetTile(_up, burningTile);
                /*  burningObject.Add(new Burn(_right, burnedTilemap, replaceable, burningTile, fire, positionRight, oneTime));
                  burningObject.Add(new Burn(_left, burnedTilemap, replaceable, burningTile, fire, positionLeft, oneTime));
                  burningObject.Add(new Burn(_up, burnedTilemap, replaceable, burningTile, fire, positionUp, oneTime));
                  burningObject.Add(new Burn(_down, burnedTilemap, replaceable, burningTile, fire, positionDown, oneTime));
                  BurningGroundPropagation.burnTile = burningObject;
                  */
                GameObject.Instantiate(fire, positionUp, Quaternion.Euler(new Vector3(-90, 0, 0)));
                _up += Vector3Int.up; // décalage pour la propagation
                _down = -_up; // mise a jour de la position pour toute les autre fonction
                _right = _up;
                _left = _up;
                positionUp += new Vector3(0, 0.32f, 0);
                positionDown = positionUp;
                positionLeft = positionUp;
                positionRight = positionUp;
                continuer = true;
                Debug.Log(_up);
            }

            if (replaceable.HasTile(_down + Vector3Int.down)) // vérifie s'il y a une tile a bruler en dessous
            {
                replaceable.SetTile(_down, null);
                burnedTilemap.SetTile(_down, burningTile);
                /* burningObject.Add(new Burn(_right, burnedTilemap, replaceable, burningTile, fire, positionRight, oneTime));
                 burningObject.Add(new Burn(_left, burnedTilemap, replaceable, burningTile, fire, positionLeft, oneTime));
                 burningObject.Add(new Burn(_up, burnedTilemap, replaceable, burningTile, fire, positionUp, oneTime));
                 burningObject.Add(new Burn(_down, burnedTilemap, replaceable, burningTile, fire, positionDown, oneTime));
                 BurningGroundPropagation.burnTile = burningObject;*/
                GameObject.Instantiate(fire, positionDown, Quaternion.Euler(new Vector3(-90, 0, 0)));
                _down += Vector3Int.down; // décalage pour la propagation
                _left = _down; // mise a jour de la position pour toute les autre fonction 
                _up = _down;
                _right = _down;
                positionDown += new Vector3(0, -0.32f, 0);
                positionLeft = positionDown;
                positionRight = positionDown;
                positionUp = positionDown;
                continuer = true;
                Debug.Log(_down);
            }
        }
        return continuer;
    }
}
