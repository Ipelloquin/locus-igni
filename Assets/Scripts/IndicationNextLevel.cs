﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicationNextLevel : MonoBehaviour
{
    public int ennemieNumber;

    public Sprite finishLevel;

    public GameObject lightDoor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ennemieNumber = GameObject.FindGameObjectsWithTag("ennemie").Length; // nombre d'ennemi dans le niveau
        
        if (ennemieNumber <= 0) // change la lumière et le sprite quand il n'y a plus d'ennemi
        {
            lightDoor.GetComponent<Light>().color = Color.green;
            GetComponent<SpriteRenderer>().sprite = finishLevel;
        }
        
    }
}
