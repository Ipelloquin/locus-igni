﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuStart : MonoBehaviour
{
    public GameObject levelMusic;
    public void NewGame ()
    {
        
        SceneManager.LoadScene("level1");
        levelMusic.SetActive(true);
    }

    public void Save()
    {
        if (PlayerPrefs.HasKey("ActiveScene"))
        {
            SceneManager.LoadScene(PlayerPrefs.GetInt("ActiveScene"));
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
